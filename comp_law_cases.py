import csv
from bs4 import BeautifulSoup
import pandas as pd
import MySQLdb
import re

import getpass
p = getpass.getpass()

connection = MySQLdb.connect(host="127.0.0.1",user="tfv338", passwd=p ,db="echr_copy",port=3306, charset='utf8',  use_unicode=True)
with connection:
    cur = connection.cursor()
    df1 = pd.read_csv('comparative law EUCtHR.csv', sep=',', skip_blank_lines = True)
    

    for i,x in df1.iterrows():   
        #print x
        caseno = x['Case Number']
        date = x['Document date']
        date = date.split('-')
        date = '-'.join([date[2],date[1],date[0]])
        print caseno,date
        cur.execute('select path from echr_copy.case_doc cd where cd.case_id = %s and cd.lang = "ENG" and (cd.doctype= "JUDGMENT" OR cd.doctype= "GC_JUDGMENT" ) and date = %s', (caseno,date))
        path= cur.fetchall()[0][0]
        print path
        soup = BeautifulSoup(open(path))
        style = soup.style.get_text()
        body = soup.body.get_text()
        text= re.sub(style, '', body).encode('utf8')
        caseno = re.sub('/', '_', caseno)
        with open('all_judgments/%s.txt' %(caseno), 'wb') as f:
            f.write(text)
        if int(date.split('-')[0]) < 2007:
            with open('1984_2006/%s.txt' %(caseno), 'wb') as s:
                s.write(text)
        else:
            with open('2007_2013/%s.txt'%(caseno), 'wb') as b:
                b.write(text)
